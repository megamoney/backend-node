'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const LinkedinStrategy = require('passport-linkedin').Strategy;
const config = require('../');
const User = mongoose.model('User');
const Resume = mongoose.model('Resume');

/**
 * Expose
 */

module.exports = new LinkedinStrategy({
        consumerKey: config.linkedin.clientID,
        consumerSecret: config.linkedin.clientSecret,
        callbackURL: config.linkedin.callbackURL,
        profileFields: ['id', 'first-name', 'last-name', 'email-address', 'positions', 'location', 'summary', 'specialties', 'headline', 'picture-url']
    },
    function (accessToken, refreshToken, profile, done) {
        const options = {
            criteria: { 'linkedin.id': profile.id }
        };
        User.loadPopulate(options, function (err, user) {
            if (err) return done(err);
            if (!user) {
                const resume = new Resume({
                    summary: profile._json.summary,
                    location: profile._json.location,
                    picture: profile._json.pictures,
                    headline: profile._json.headline,
                    positions: profile._json.positions
                });

                user = new User({
                    name: profile.displayName,
                    email: profile.emails[0].value,
                    username: profile.emails[0].value,
                    provider: 'linkedin',
                    linkedin: profile._json,
                    avatar: profile._json.pictureUrl,
                    resume: resume
                });

                resume.user = user.id;

                user.save().then(function () {
                    return resume.save();
                }).then(function () {
                    return done(err, user);
                }).catch(function (err) {
                    if (err) console.log(err);

                    return done(new Error('Promise error when saving user!'));
                });
            } else {
                return done(err, user);
            }
        });
    }
);
