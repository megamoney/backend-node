'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const LinkedInTokenStrategy = require('passport-linked-in-token');
const config = require('../');
const User = mongoose.model('User');

/**
 * Expose
 */

module.exports = new LinkedInTokenStrategy({
        clientID: config.linkedin.clientID,
        clientSecret: config.linkedin.clientSecret,
        scope: ['r_emailaddress', 'r_basicprofile'],
        profileFields: ['id', 'first-name', 'last-name', 'email-address', 'positions', 'summary', 'specialties', 'headline', 'picture-url', 'location:(name,country:(code))'], // idk why location will result in a 500
        passReqToCallback: true
    },
    function (req, accessToken, refreshToken, profile, done) {
        const options = {
            criteria: { 'linkedin.id': profile.id }
        };
        User.loadPopulate(options, function (err, user) {
            if (err) return done(err);
            if (!user) {
                user = new User({
                    name: profile.displayName,
                    email: profile.emails[0].value,
                    username: profile.emails[0].value,
                    avatar: profile._json.pictureUrl,
                    provider: 'linkedin',
                    linkedin: profile._json,
                    resume: resume
                });

                user.save().then(function () {
                    return done(err, user);
                }).catch(function (err) {
                    if (err) console.log(err);

                    return done(new Error('Promise error when saving user!'));
                });
            } else {
                return done(err, user);
            }
        });
    }
);
