'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const GoogleIdTokenStrategy = require('passport-google-id-token');
const config = require('../');
const User = mongoose.model('User');

/**
 * Expose
 */

module.exports = new GoogleIdTokenStrategy({
        clientID: config.google.clientID
    },
    function (accessToken, googleId, done) {
        const profile = accessToken.payload;

        if (config.google.allowedAUDs.indexOf(profile.aud) === -1) {
            return done(new Error('The AUD from response doensn\'t match any AUD specified in the whitelist'));
        }

        const options = {
            criteria: { 'google.id': googleId }
        };

        User.load(options, function (err, user) {
            if (err) return done(err);
            if (!user) {
                user = new User({
                    name: profile.name,
                    email: profile.email,
                    username: profile.email,
                    provider: 'google',
                    google: profile
                });
                user.save(function (err) {
                    if (err) console.log(err);
                    return done(err, user);
                });
            } else {
                return done(err, user);
            }
        });
    }
);
