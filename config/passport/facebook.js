'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const FacebookStrategy = require('passport-facebook').Strategy;
const config = require('../');
const User = mongoose.model('User');

/**
 * Expose
 */

module.exports = new FacebookStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: config.facebook.callbackURL,
        profileFields: ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified', 'picture']
    },
    function (accessToken, refreshToken, profile, done) {
        const options = {
            criteria: { 'facebook.id': profile.id }
        };
        User.load(options, function (err, user) {
            if (err) return done(err);
            if (!user) {
                let profile_email = 'test@test.com';
                if (profile.emails) {
                    profile_email = profile.emails[0].value;
                }

                user = new User({
                    name: profile.displayName,
                    email: profile_email,
                    username: profile.username,
                    avatar: profile._json.picture.data.url,
                    provider: 'facebook',
                    facebook: profile._json
                });
                user.save(function (err) {
                    if (err) console.log(err);
                    return done(err, user);
                });
            } else {
                return done(err, user);
            }
        });
    }
);
