'use strict';

/*
 * Module dependencies.
 */

const users = require('../app/controllers/users');
const articles = require('../app/controllers/articles');
const comments = require('../app/controllers/comments');
const tags = require('../app/controllers/tags');
const auth = require('./middlewares/authorization');
const mailer = require('../app/mailer');

/**
 * Route middlewares
 */

const articleAuth = [auth.requiresLogin, auth.article.hasAuthorization];
const commentAuth = [auth.requiresLogin, auth.comment.hasAuthorization];

const fail = {
    failureRedirect: '/login'
};

/**
 * Expose routes
 */

module.exports = function (app, passport, autocomplete, upload) {
    const pauth = passport.authenticate.bind(passport);

    // user routes
    app.get('/login', users.login);
    app.get('/signup', users.signup);
    app.get('/logout', users.logout);
    app.post('/users', users.create, users.authCallback);
    app.post('/users/session',
        pauth('local', {
            // failureRedirect: '/login',
            failureFlash: 'Invalid email or password.'
        }), users.session);

    // users routes
    app.get('/users/profile/:userId', users.show);

    app.get('/auth/facebook',
        pauth('facebook', {
            scope: ['email', 'public_profile'],
            // failureRedirect: '/login'
        }), users.signin);
    app.get('/auth/facebook/token', pauth('facebook-token', fail), users.authCallback);
    app.get('/auth/facebook/callback', pauth('facebook', fail), users.authCallback);

    // app.get('/auth/github', pauth('github', fail), users.signin);
    // app.get('/auth/github/callback', pauth('github', fail), users.authCallback);
    // app.get('/auth/twitter', pauth('twitter', fail), users.signin);
    // app.get('/auth/twitter/callback', pauth('twitter', fail), users.authCallback);

    app.get('/auth/google',
        pauth('google', {
            // failureRedirect: '/login',
            scope: [
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email'
            ]
        }), users.signin);
    app.post('/auth/google/token', pauth('google-id-token', fail), users.authCallback);
    app.get('/auth/google/callback', pauth('google', fail), users.authCallback);

    app.get('/auth/linkedin',
        pauth('linkedin', {
            // failureRedirect: '/login',
            scope: [
                'r_basicprofile',
                'r_emailaddress'
            ]
        }), users.signin);
    app.get('/auth/linkedin/token', pauth('linkedin-token', fail), users.liCallback);
    app.get('/auth/linkedin/callback', pauth('linkedin', fail), users.liCallback);

    app.param('userId', users.load);

    app.post('/users/setup/finish', auth.requiresLogin, users.setupFinished);
    app.post('/users/card/ocr', upload.single('card'), users.doOCR);

    app.get('/users/transaction/pdf', users.pdf);

    app.get('/invoice/confirm/:redisId', function (res, req) {
        res.end('bye');
    });

    app.param('redisId', users.order);

    app.get('/skills/search', /* auth.requiresLogin, */function (req, res) {
        if (!req.query.q) {
            res.json({
                'status': 'error',
                'message': 'No query param defined'
            });

            return;
        }

        let matches = autocomplete.search(req.query.q.toLowerCase(), !req.query.limit ? undefined : req.query.limit);


        //res.json(JSON.parse(json));
    });

    // article routes
    app.param('id', articles.load);
    app.get('/articles', articles.index);
    app.get('/articles/new', auth.requiresLogin, articles.new);
    app.post('/articles', auth.requiresLogin, articles.create);
    app.get('/articles/:id', articles.show);
    app.get('/articles/:id/edit', articleAuth, articles.edit);
    app.put('/articles/:id', articleAuth, articles.update);
    app.delete('/articles/:id', articleAuth, articles.destroy);

    // home route
    app.get('/', articles.index);

    // comment routes
    app.param('commentId', comments.load);
    app.post('/articles/:id/comments', auth.requiresLogin, comments.create);
    app.get('/articles/:id/comments', auth.requiresLogin, comments.create);
    app.delete('/articles/:id/comments/:commentId', commentAuth, comments.destroy);

    // tag routes
    app.get('/tags/:tag', tags.index);

    // mare test
    app.get('/test', function (req, res) {
        mailer.register({
            email: 'alx.ionescu97@gmail.com',
            name: 'Alexandru Ionescu',
        }, function () {
            console.log('Email sent at: ' + new Date().getTime());
        });

        res.end('sal');
    });


    /**
     * Error handling
     */

    app.use(function (err, req, res, next) {
        // treat as 404
        if (err.message
            && (~err.message.indexOf('not found')
                || (~err.message.indexOf('Cast to ObjectId failed')))) {
            return next();
        }

        // console.error(err.message);
        console.error(err.stack);

        if (err.stack.includes('ValidationError')) {
            res.status(422).render('422', { error: err.stack });
            return;
        }

        if (err.stack.includes('AccountActivationError')) {
            res.status(err.status).render('error', { error: err.stack });
            return;
        }

        // error page
        res.status(500).render('500', { error: err.stack });
    });

    // assume 404 since no middleware responded
    app.use(function (req, res) {
        const payload = {
            url: req.originalUrl,
            error: 'Not found'
        };
        if (req.accepts('json')) return res.status(404).json(payload);
        res.status(404).render('404', payload);
    });
};
