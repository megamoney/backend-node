'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const User = mongoose.model('User');

const local = require('./passport/local');
const google = require('./passport/google');
const google_token = require('./passport/google-token');
const facebook = require('./passport/facebook');
const facebook_token = require('./passport/facebook-token');
// const twitter = require('./passport/twitter');
// const linkedin = require('./passport/linkedin');
// const linkedin_token = require('./passport/linkedin-token');
// const github = require('./passport/github');

/**
 * Expose
 */

module.exports = function (passport) {

    // serialize sessions
    passport.serializeUser((user, cb) => cb(null, user.id));
    passport.deserializeUser((id, cb) => User.load({ criteria: { _id: id } }, cb));

    // use these strategies
    passport.use(local);
    passport.use(google);
    passport.use(google_token);
    // passport.use(facebook);
    // passport.use(facebook_token);
    // passport.use(twitter);
    // passport.use(linkedin);
    // passport.use(linkedin_token);
    // passport.use(github);
};
