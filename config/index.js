'use strict';

/**
 * Module dependencies.
 */

const path = require('path');

const development = require('./env/development');
const test = require('./env/test');
const test_vps = require('./env/vps');
const production = require('./env/production');

const mailer = {
    appLogoPath: path.join(__dirname, '..', 'public/img/voda.png'),
    viewRoot: path.join(__dirname, '..', 'app/mailer/emails')
};

const defaults = {
    root: path.join(__dirname, '..'),
    mailer: mailer
};

/**
 * Expose
 */

module.exports = {
    development: Object.assign({}, development, defaults),
    test_vps: Object.assign({}, test_vps, defaults),
    test: Object.assign({}, test, defaults),
    production: Object.assign({}, production, defaults)
}[process.env.NODE_ENV || 'development'];
