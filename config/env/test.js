'use strict';

/**
 * Expose
 */

module.exports = {
    matchStatus: {
        acceptedByUser: 'acceptedByUser',
        acceptedByRecruiter: 'acceptedByRecruiter',
        declined: 'declined',
        pending: 'pending',
    },
    db: `mongodb://${process.env.MONGODB_USER}:${process.env.MONGODB_PASS}@${process.env.MONGODB_HOST}`,
    email: {
        user: process.env.MAILER_EMAIL,
        pass: process.env.MAILER_PASSWORD,
        activationURL: 'http://localhost:3000/users/activate/'
    },
    facebook: {
        clientID: process.env.FACEBOOK_CLIENTID,
        clientSecret: process.env.FACEBOOK_SECRET,
        callbackURL: 'http://bcreaderapp.com:3000/auth/facebook/callback'
    },
    twitter: {
        clientID: process.env.TWITTER_CLIENTID,
        clientSecret: process.env.TWITTER_SECRET,
        callbackURL: 'http://bcreaderapp.com:3000/auth/twitter/callback'
    },
    github: {
        clientID: process.env.GITHUB_CLIENTID,
        clientSecret: process.env.GITHUB_SECRET,
        callbackURL: 'http://bcreaderapp.com:3000/auth/github/callback'
    },
    linkedin: {
        clientID: process.env.LINKEDIN_CLIENTID,
        clientSecret: process.env.LINKEDIN_SECRET,
        callbackURL: 'http://bcreaderapp.com:3000/auth/linkedin/callback'
    },
    google: {
        clientID: process.env.GOOGLE_CLIENTID,
        clientSecret: process.env.GOOGLE_SECRET,
        allowedAUDs: process.env.GOOGLE_ALLOWED_AUD.split(' '),
        callbackURL: 'http://bcreaderapp.com:3000/auth/google/callback'
    }
};
