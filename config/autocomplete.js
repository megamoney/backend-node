const mongoose = require('mongoose');
// const Skill = mongoose.model('Skill');

const Autocomplete = require('autocomplete-trie');
// Create the autocomplete object
const auto = new Autocomplete();

const actual_values = [];

/**
 * Expose routes
 */

module.exports = {
    initialize: function () {
        // Bootstrap auto complete
        /* Skill.loadAll({}, function (err, skillsDocument) {
            if (err) {
                console.error(err);
                return;
            }

            if (!skillsDocument) {
                console.error('No skills found in the DB... this is highly impossible');
                return;
            }

            const skills = skillsDocument.map(elem => {
                let key = elem.technology.toLowerCase();
                actual_values[key] = elem.technology;

                return key;
            });

            auto.initialize(skills);
        });*/
    },
    search: function (keyword, limit = 10) {
        let matches = auto.search(keyword);

        matches = matches.map(elem => {
            return actual_values[elem];
        });

        return matches.slice(0, limit > matches.length ? matches.length : limit);
    }
};