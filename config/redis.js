'use strict';

/**
 * Module dependencies.
 */

const redis = require('redis').createClient();

module.exports = redis;