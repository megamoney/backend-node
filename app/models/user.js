'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const mailer = require('../mailer');

const saltRounds = 11;

const Schema = mongoose.Schema;
const oAuthTypes = [
    'github',
    'twitter',
    'facebook',
    'google',
    'linkedin'
];

const re = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+.)+[^<>()[\].,;:\s@"]{2,})$/i;

/**
 * User Schema
 */

const UserSchema = new Schema({
    name: { type: String, default: '' },
    email: { type: String, default: '' },
    username: { type: String, default: '' },
    provider: { type: String, default: '' },
    avatar: { type: String, default: '' },
    hashed_password: { type: String, default: '' },
    authToken: { type: String, default: '' },
    resume: { type: Schema.Types.ObjectId, ref: 'Resume' },
    setup_finished: { type: Boolean, default: false },
    account_activated: { type: Boolean, default: false },
    facebook: {},
    twitter: {},
    github: {},
    google: {},
    linkedin: {}
});

const validatePresenceOf = value => value && value.length;

/**
 * Virtuals
 */

UserSchema
    .virtual('password')
    .set(function (password) {
        this._password = password;
    })
    .get(function () {
        return this._password;
    });

/**
 * Validations
 */

// the below 5 validations only apply if you are signing up traditionally

UserSchema.path('name').validate(function (name) {
    if (this.skipValidation()) return true;
    return name.length;
}, 'Name cannot be blank');

UserSchema.path('email').validate(function (email) {
    if (this.skipValidation()) return true;
    return email.length && this.validateEmail(email);
}, 'Invalid email address');

UserSchema.path('email').validate({
    isAsync: true,
    validator: function (email, callback) {
        const User = mongoose.model('User');
        if (this.skipValidation()) {
            callback(true);
        }

        // Check only when it is a new user or when email field is modified
        if (this.isNew || this.isModified('email')) {
            return User.find({ email: email }).exec(function (err, users) {
                callback(!err && users.length === 0);
            });
        } else {
            callback(true);
        }
    }, message: 'Email already exists'
});

UserSchema.path('username').validate(function (username) {
    if (this.skipValidation()) return true;
    return username.length;
}, 'Username cannot be blank');

UserSchema.path('hashed_password').validate(function (hashed_password) {
    if (this.skipValidation()) return true;
    return hashed_password.length && this._password.length;
}, 'Password cannot be blank');


/**
 * Pre-save hook
 */

UserSchema.pre('save', function (next) {
    if (!this.isNew) return next();

    if (this.skipValidation()) {
        this.account_activated = true;
    }

    if (!validatePresenceOf(this.password) && !this.skipValidation()) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});

UserSchema.pre('validate', function preValidate (next) {
    return this.encryptPassword(this._password).then(next);
});

UserSchema.post('save', function (user, next) {
    if (this.skipValidation()) {
        return next();
    }

    mailer.register({
        email: user.email,
        name: user.name,
        userId: user._id
    }, function () {
        console.log('Email sent at: ' + new Date().getTime());
    });

    console.log('here we should send the verification email');
    return next();
});

/**
 * Methods
 */

UserSchema.methods = {

    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @param cb callback function indicating the result of the check
     * @api public
     */

    authenticate: function (plainText, cb) {
        bcrypt.compare(plainText, this.hashed_password).then(cb);
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */

    encryptPassword: async function (password) {
        if (!password) return '';
        try {
            this.hashed_password = await bcrypt.hash(password, saltRounds).then(function (hash) {
                return hash;
            });

            return '';
        } catch (err) {
            return '';
        }
    },

    /**
     * Validation is not required if using OAuth
     */

    skipValidation: function () {
        return ~oAuthTypes.indexOf(this.provider);
    },

    /**
     *
     * Validates an email address against the regex
     * @param email the email address that should be checked
     * @returns {boolean} the result of this laborious operation
     */
    validateEmail: function (email) {
        return re.test(String(email).toLowerCase());
    }
};

/**
 * Statics
 */

UserSchema.statics = {

    /**
     * Load
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */
    load: function (options, cb) {
        options.select = options.select || 'name username setup_finished avatar';
        return this.findOne(options.criteria)
            .select(options.select)
            .exec(cb);
    },

    loadPopulate: function (options, cb) {
        options.select = options.select || 'name username setup_finished avatar';
        options.populate = options.populate || 'resume';

        return this.findOne(options.criteria)
            .populate({ path: options.populate, select: 'user positions location skills headline picture summary' })
            .select(options.select)
            .exec(cb);
    },

    update: function (options, cb) {
        options.update = options.update || { 'setup_finished': true };
        return this.updateOne(options.criteria, { $set: options.update })
            .exec(cb);

    },

    updateOrCreateField: function (options, cb) {
        options.update = options.update || { 'setup_finished': true };
        return this.updateOne(options.criteria, options.update, { upsert: true })
            .exec(cb);
    }
};

mongoose.model('User', UserSchema);
