module.exports = class AccountActivationError extends require('./AppError') {
    constructor (message) {
        // Providing default message and overriding status code.
        super(message || 'Couldn\'t activate your account', 400);
    }
};