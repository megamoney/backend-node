const config = require('../../config');

module.exports = {
    respond,
    respondOrRedirect,
    pdfHeader,
    pdfFooter,
    pdfAddress,
    pdfPersonalInfo,
    parseOCRJson
};

function respond(res, tpl, obj, status) {
    res.format({
        html: () => res.render(tpl, obj),
        json: () => {
            if (status) return res.status(status).json(obj);
            res.json(obj);
        }
    });
}

function respondOrRedirect({req, res}, url = '/', obj = {}, flash) {
    res.format({
        html: () => {
            if (req && flash) req.flash(flash.type, flash.text);
            res.redirect(url);

        },
        json: () => res.json(obj)
    });
}

function pdfHeader(doc, profile) {
    const initial_y = doc.y;
    doc.image(profile.profile_pic, 50, initial_y, {
        width: 60
    });

    doc.x = 120;
    doc.y = initial_y;

    doc.fontSize(24);
    doc.text(profile.name, {
        paragraphGap: 5
    });

    doc.fontSize(12);
    doc.text(profile.serie_ci, {
        paragraphGap: 5
    });

    doc.fontSize(12);
    doc.text(profile.title, {
        paragraphGap: 5
    });

    doc.moveTo(60, doc.y)
        .lineTo(doc.page.width - 30, doc.y)
        .fill('#e63000');

    doc.y += 10;
}

function pdfFooter(doc) {
    doc.image(config.mailer.appLogoPath, (doc.page.width - 100) / 2, doc.page.height - 40, {
        width: 100
    });

    doc.moveTo(60, doc.page.height - 50)
        .lineTo(doc.page.width - 30, doc.page.height - 50)
        .fill('#e63000');
}

function pdfAddress(doc, parsed_json) {
    doc.x = 50;
    const addresses = parsed_json['address'];

    // draw user skills
    if (addresses && addresses.length > 0) {
        doc.fontSize(16);
        doc.text('Adresa Livrare', {
            paragraphGap: 5
        });

        doc.fontSize(12)
            .fill('black');

        addresses.forEach((address) => {
            doc.text(address.normalize());
        });

        doc.moveTo(60, doc.y)
            .lineTo(doc.page.width - 30, doc.y)
            .fill('#e63000');

        doc.y += 10;
    }
}

function pdfPersonalInfo(doc, parsed_json) {
    doc.x = 50;

    doc.fontSize(16);
    doc.text('Informatii Personale', {
        paragraphGap: 5
    });

    doc.fontSize(12)
        .fill('black');


    doc.text(`CNP - ${parsed_json['mrz']['second_line']['cnp']}`);

    doc.text(`Nationalitate - ${parsed_json['mrz']['second_line']['nationality']}`);

    if (parsed_json['mrz']['second_line']['sex'] === 'M') {
        doc.text('Sex - Barbat');
    } else {
        doc.text('Sex - Femeie');
    }

    doc.moveTo(60, doc.y)
        .lineTo(doc.page.width - 30, doc.y)
        .fill('#e63000');

    doc.y += 10;
}

let first_line = [];
first_line[0] = 'last_name';
first_line[1] = 'first_name1';
first_line[2] = 'first_name2';

function parseMRZLine(line) {
    if (line.toUpperCase().startsWith('IDROU')) { //first line
        line = line.substr(5, line.length);

        let field = '';
        let fieldID = 0;
        let res = {};

        for (let i = 0; i < line.length; i++) {

            if (line.charAt(i) === '<') {
                if (field !== '') {
                    res[first_line[fieldID]] = field;
                    fieldID++;
                }

                field = '';
                continue;
            }

            field += line.charAt(i);
        }

        return {'first_line': res};
    } else { //other one
        const seriesCode = line.substr(0, 2);
        const seriesNumber = line.substr(2, 6);

        const checkDigit1 = line.charAt(9) + '';

        const nationality = line.substr(10, 3);
        const birthday = line.substr(17, 2) + '.' + line.substr(15, 2) + '.' + line.substr(13, 2);

        const checkDigit2 = line.charAt(19) + '';

        const sex = line.charAt(20) + '';
        const expiryDate = line.substr(25, 2) + '.' + line.substr(23, 2) + '.' + line.substr(21, 2);

        const checkDigit3 = line.charAt(27) + '';

        const personalNumber = line.substr(28, 7);

        const finalCheckDigit = line.charAt(35) + '';

        const cnp = personalNumber.charAt(0) + line.substr(13, 2) + line.substr(15, 2) + line.substr(17, 2) + personalNumber.substr(1);

        return {
            'second_line': {
                'seriesCode': seriesCode,
                'seriesNumber': seriesNumber.replace('O', '0'),
                'checkDigit1': checkDigit1,
                'nationality': nationality,
                'birthday': birthday,
                'checkDigit2': checkDigit2,
                'sex': sex,
                'expiryDate': expiryDate,
                'checkDigit3': checkDigit3,
                'personalNumber': personalNumber,
                'finalCheckDigit': finalCheckDigit,
                'cnp': cnp,
            }
        };
    }
}

function parseMRZ(json) {
    const jobject = JSON.parse(json);
    if (jobject['status'] === 'error') {
        return 'error';
    }

    if (!jobject['fields']['mrz']) {
        return 'No MRZ fields detected';
    }

    jobject['fields']['mrz'].forEach((mrzField, index, arr) => {
        arr[index] = mrzField.replace(/[ \t\r\n]+/g, '');
    });

    if (jobject['fields']['mrz'].length === 2) {
        return Object.assign(parseMRZLine(jobject['fields']['mrz'][0]), parseMRZLine(jobject['fields']['mrz'][1]));
    } else if (jobject['fields']['mrz'].length === 1) {
        return Object.assign(parseMRZLine(jobject['fields']['mrz'][0]));
    } else {
        return 'err';
    }
}

let location_keywords = ['bld.', 'ale.', 'str.', 'mun.', 'bld,', 'ale,', 'str,', 'mun,'];

function contains_keyword(line, list) {
    for (let i = 0; i < list.length; i++) {
        let idx = line.toLowerCase().indexOf(list[i]);
        if (idx !== -1) {
            return line.substr(idx);
        }
    }

    return '';
}

function parseOther(json) {
    const jobject = JSON.parse(json);
    if (jobject['status'] === 'error') {
        return 'error';
    }

    if (!jobject['fields']['other']) {
        return 'No Other fields detected';
    }

    let candidates = [];
    jobject['fields']['other'].forEach((otherField, index, arr) => {
        let sub_arr = otherField.split('\n');

        for (let i = 0; i < sub_arr.length; i++) {
            let result = contains_keyword(sub_arr[i], location_keywords);

            if (result !== '') {
                candidates.push(result);
            }
        }
    });

    candidates.sort(function (a, b) {
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        return b.length - a.length;
    });

    return candidates;
}

function parseOCRJson(json) {
    const MRZ = parseMRZ(json);

    const addrs = parseOther(json);

    const jobject = JSON.parse(json);

    const i_photo = jobject['file']['original_file'].lastIndexOf('/');
    const res_photo = jobject['file']['original_file'].substr(i_photo);

    const i_dot = res_photo.indexOf('.');
    const filename = res_photo.substr(0, i_dot);

    const i_face = jobject['file']['face_file'].lastIndexOf('/');
    const res_face = jobject['file']['face_file'].substr(i_face);

    return {
        'mrz': MRZ,
        'address': addrs,
        'files': {
            'face': '/public/uploads' + res_face,
            'card': '/public/uploads' + res_photo,
            'pdf': '/public/documents' + filename + '.pdf',
        }
    };
}