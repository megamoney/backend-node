'use strict';

/**
 * Module dependencies.
 */

const Email = require('email-templates');
const nodemailer = require('nodemailer');
const config = require('../../config');

const cachePugTemplates = require('cache-pug-templates');
const redisClient = require('../../config/redis');

const crypto = require('crypto');

/*
const mailTemplate = new EmailTemplate(config.mailer.commentTemplate);
*/

const transporter = nodemailer.createTransport({
    host: 'smtp.zoho.eu',
    port: 465,
    secure: true, // ssl
    auth: {
        user: config.email.user,
        pass: config.email.pass
    }
});

let email = new Email({
    message: {
        from: config.email.user,
        attachments: [{
            filename: 'voda.png',
            path: config.mailer.appLogoPath,
            cid: 'app-logo'
        }],
    },
    // uncomment below to send emails in development/test env:
    send: true,
    transport: transporter,
    juiceResources: {
        webResources: {
            images: false,
        },
    },
    views: {
        root: config.mailer.viewRoot
    }
});

cachePugTemplates(redisClient, config.mailer.viewRoot, (err, cached) => {
    if (err) throw err;
    console.log(`successfully cached (${cached.length}) files`);
});

/**
 * Expose
 */

module.exports = {

    /**
     * Send email to those who sign up using email/pass
     * Generate 20 chars random string and store it using Redis for a specified amount of time
     *
     * @param {Object} options
     * @param {Function} cb
     * @api public
     */

    register: function (options, cb) {
        let token = crypto.randomBytes(20).toString('hex');

        let locals = {
            to: options.email,
            name: options.name,
            verification_url: (config.email.activationURL + token)
        };

        console.log('Begin send mail at: ' + new Date().getTime());
        email
            .send({
                template: 'register',
                message: {
                    to: options.email
                },
                locals: locals
            })
            .then(function (html) {
                let uid = options.email.toString();
                console.log('Adding into redis: ', token, '=>', uid);
                redisClient.set(token, uid, 'EX', 60 * 60); // 1h
                return html;
            })
            .then(cb)
            .catch(console.error);
    },

    invoice: function (options, cb) {
        email = new Email({
            message: {
                from: config.email.user,
                attachments: [{
                    filename: 'voda.png',
                    path: config.mailer.appLogoPath,
                    cid: 'app-logo'
                }, {
                    filename: 'invoice.pdf',
                    path: options.pdf_path,
                    cid: 'app-invoice'
                }],
            },
            // uncomment below to send emails in development/test env:
            send: true,
            transport: transporter,
            juiceResources: {
                webResources: {
                    images: false,
                },
            },
            views: {
                root: config.mailer.viewRoot
            }
        });

        let token = crypto.randomBytes(20).toString('hex');

        let locals = {
            to: options.email,
            name: options.name,
        };

        console.log('Begin send mail at: ' + new Date().getTime());
        email
            .send({
                template: 'invoice',
                message: {
                    to: options.email
                },
                locals: locals
            })
            .then(function (html) {
                let uid = options.email;
                console.log('Adding into redis: ', token, '=>', uid);
                redisClient.set(token, uid, 'EX', 60 * 60); // 1h
                return html;
            })
            .then(cb)
            .catch(console.error);
    },

    confirmation: function (options, cb) {
        email = new Email({
            message: {
                from: config.email.user,
                attachments: [{
                    filename: 'voda.png',
                    path: config.mailer.appLogoPath,
                    cid: 'app-logo'
                }],
            },
            // uncomment below to send emails in development/test env:
            send: true,
            transport: transporter,
            juiceResources: {
                webResources: {
                    images: false,
                },
            },
            views: {
                root: config.mailer.viewRoot
            }
        });

        let token = crypto.randomBytes(20).toString('hex');

        let locals = {
            to: options.email,
            name: options.name,
            verification_url: (config.email.activationURL + token)
        };

        console.log('Begin send mail at: ' + new Date().getTime());
        email
            .send({
                template: 'confirmation',
                message: {
                    to: options.email
                },
                locals: locals
            })
            .then(function (html) {
                let uid = options.email;
                console.log('Adding into redis: ', token, '=>', uid);
                redisClient.set(token, JSON.stringify({
                    'email': options.email,
                    'json': options.json_path
                }), 'EX', 60 * 60); // 1h
                return html;
            })
            .then(cb)
            .catch(console.error);
    }
};
