'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const { wrap: async } = require('co');
const { pdfHeader, pdfFooter, pdfAddress, pdfPersonalInfo } = require('../utils');
const User = mongoose.model('User');

const { exec } = require('child_process');
const config = require('../../config/.');
const fs = require('fs');

const redisClient = require('../../config/redis');
const AccountActivationError = require('../errors/AccountActivationError');

const PDFDocument = require('pdfkit');

const utils = require('../utils/.');
const mailer = require('../../app/mailer');

/**
 * Activate user account
 */

exports.activate = function (req, res, next, token) {
    console.log('Activating with token: ', token);

    redisClient.get(token, function (err, userId) {
        if (err) {
            return next(err);
        }

        redisClient.del(token);

        if (!userId) {
            return next(new AccountActivationError('Invalid or expired activation token'));
        }

        const uoptions = {
            criteria: {
                '_id': userId,
                'provider': 'local'
            },
            update: {
                'account_activated': true
            }
        };

        User.update(uoptions, function (err) {
            if (err) {
                return next(new AccountActivationError('Couldn\'t activate your account'));
            }

            next();
        });
    });
};

/**
 * Generate user PDF profile
 */
function generatePDF(raw_json, parsed_json) {
    const doc = new PDFDocument({
        size: 'A4',
        margins: {
            top: 30,
            left: 60,
            bottom: 30,
            right: 30
        }
    });

    const i_pdf = parsed_json['files']['pdf'].lastIndexOf('/');
    const pdf = parsed_json['files']['pdf'].substr(i_pdf + 1);

    const i_json = parsed_json['files']['pdf'].lastIndexOf('/');
    let json = parsed_json['files']['pdf'].substr(i_json + 1);
    json = json.substr(0, json.length - 4);


    console.log(config.root + '/public/documents/' + pdf);
    doc.pipe(fs.createWriteStream(config.root + '/public/documents/' + pdf));

    parsed_json['actual_pdf_path'] = config.root + '/public/documents/' + pdf;
    parsed_json['actual_json_path'] = config.root + '/../uploads/' + json + '.json';

    console.log('Default font size: %f', doc._fontSize);

    pdfHeader(doc, {
        profile_pic: raw_json['file']['face_file'],
        name: parsed_json['mrz']['first_line']['last_name'] + ' ' + parsed_json['mrz']['first_line']['first_name1'] + ' ' + parsed_json['mrz']['first_line']['first_name2'],
        title: 'Contract de vanzare-cumparare mobile',
        serie_ci: parsed_json['mrz']['second_line']['seriesCode'] + ' ' + parsed_json['mrz']['second_line']['seriesNumber']
    });

    pdfAddress(doc, parsed_json);
    pdfPersonalInfo(doc, parsed_json);
    // pdfUserSkills(doc, user);

    // pdfUserExperience(doc, user);

    doc.fontSize(12);

    /*
    let loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in...';

    doc.y = 320;
    doc.fillColor('black');
    doc.text(loremIpsum, {
        paragraphGap: 10,
        indent: 20,
        align: 'justify',
        columns: 2
    });

    console.log('current cursor: %d', doc.y);
    doc.text('salut meeen scriu si eu aici ceva ca ma plictisesc tare rau de tot si sper sa fie de ajuns sa umplu toata linia');*/

    pdfFooter(doc);

    doc.end();
}

exports.pdf = function (req, res, next) {
    res.type('application/pdf'); // If you omit this line, file will download

    /* const options = {
        criteria: { '_id': req.user.id },
        select: 'name email'
    };*/
/*
    User.loadPopulate(options, function (err, user) {
        if (err) {
            next(err);
            return;
        }

        if (!user) {
            next(new Error('User not found'));
            return;
        }

        generatePDF(req, res, user);
    });*/
    generatePDF(req, res, {
        'name': 'Alex',
        'email': 'alx.ionescu97@gmail.com',

    });
};


/**
 * Load
 */

exports.load = async(function* (req, res, next, _id) {
    const criteria = { _id };
    try {
        req.profile = yield User.load({criteria});
        if (!req.profile) return next(new Error('User not found'));
    } catch (err) {
        return next(err);
    }
    next();
});

exports.order = function (req, res, next, _id) {
    //get the value

    redisClient.get(_id, function(err, value) {
        if (err || !value) {
            console.log(err);
            return;
        }

        const obj = JSON.parse(value);
        redisClient.del(_id);

        const jsonContent = fs.readFileSync(obj.json, 'utf8');

        const parsed_json = utils.parseOCRJson(jsonContent);

        generatePDF(JSON.parse(jsonContent), parsed_json);

        mailer.invoice({
            email: obj.email,
            name: parsed_json['mrz']['first_line']['last_name'] + ' ' + parsed_json['mrz']['first_line']['first_name1'],
            pdf_path: parsed_json['actual_pdf_path']
        }, function () {
            console.log('Email sent at: ' + new Date().getTime());
        });

        res.end('bye');
    });
};

/**
 * Create user
 */

exports.create = async(function* (req, res, next) {
    const user = new User(req.body);
    user.provider = 'local';
    try {
        yield user.save();
        req.logIn(user, err => {
            if (err) return next(new Error('Sorry! We are not able to log you in!'));
            return next();
        });
    } catch (err) {
        const errors = Object.keys(err.errors)
            .map(field => err.errors[field].message);

        res.render('users/signup', {
            title: 'Sign up',
            errors,
            user
        });
    }
});

/**
 *  Show profile
 */

exports.show = function (req, res) {
    const user = req.profile;
    res.json(user);

    /* respond(res, 'users/show', {
        title: user.name,
        user: user
    }); */
};

exports.signin = function () {
    console.log('hello @ signin');
};

/**
 * Auth callback
 */

exports.authCallback = login;

/**
 * Show login form
 */

exports.login = function (req, res) {
    res.render('users/login', {
        title: 'Login'
    });
};

/**
 * Show sign up form
 */

exports.signup = function (req, res) {
    res.render('users/signup', {
        title: 'Sign up',
        user: new User()
    });
};

/**
 * Logout
 */

exports.logout = function (req, res) {
    req.logout();
    res.redirect('/login');
};

/**
 * Session
 */

exports.session = login;

/**
 * Login
 */

function login(req, res) {
    /* const redirectTo = req.session.returnTo
        ? req.session.returnTo
        : '/';

    delete req.session.returnTo;
    res.redirect(redirectTo);*/
    res.json({
        status: 'SUCCESS',
        message: 'You are now logged in'
    });
}

/**
 * Gets called when the LinkedIn authentication is successful
 * @param req
 * @param res
 */
exports.liCallback = function (req, res) {
    res.json(req.user);
};

/**
 * This route should be called when a user finishes the setup...
 * The body of this POST should contain the final user data
 * @param req
 * @param res
 */
exports.setupFinished = function (req, res) {
    // update the resume with the corresponding user id
};

exports.avatarUploaded = function (req, res) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    if (!req.file) {
        res.json({
            'status': 'Your file most probably got rejected... What were you trying to upload?'
        });
        return;
    }

    const uoptions = {
        criteria: {'_id': req.user.id},
        update: {
            'avatar': req.file.filename
        }
    };

    User.updateOrCreateField(uoptions, function (err) {
        if (err) {
            res.json({
                'status': 'Couldn\'t update the user',
                'err': err
            });
            return;
        }

        res.json({
            'status': 'Successfully updated user'
        });
    });
};

exports.getAvatar = function (req, res) {
    if (!/^(f|ht)tps?:\/\//i.test(req.user.avatar)) {
        res.redirect('/static/' + req.user.avatar);

        return;
    }

    res.redirect(req.user.avatar);
};

exports.doOCR = async function (req, res) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    const photo = config.root + '/' + req.file.path;

    console.log('Got this photo file: ', photo);

    await exec('/home/ialex/CLionProjects/carditionnative/cmake-build-debug/PerspectiveWarp ' + photo, (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            return res.json({
                'status': 'error',
                'message': 'OCR failure'
            });
        }

        const jsonContent = fs.readFileSync(stdout, 'utf8');

        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);

        const parsed_json = utils.parseOCRJson(jsonContent);

        try {
            generatePDF(JSON.parse(jsonContent), parsed_json);
        } catch (e) {
            return res.json({
                'status': 'error',
                'message': 'Field recognition failure'
            });
        }

        mailer.confirmation({
            email: req.body.email,
            name: parsed_json['mrz']['first_line']['last_name'] + ' ' + parsed_json['mrz']['first_line']['first_name1'],
            json_path: parsed_json['actual_json_path']
        }, function () {
            console.log('Email sent at: ' + new Date().getTime());
        });

        res.json(parsed_json);
    });
};