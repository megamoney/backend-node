'use strict';

/**
 * Module dependencies
 */

require('dotenv').config();

const fs = require('fs');
const join = require('path').join;
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const config = require('./config');

const http = require('http');
const https = require('https');
const privateKey = fs.readFileSync('ssl/localhost.pem');
const certificate = fs.readFileSync('ssl/localhost.crt');
const options = {key: privateKey,cert: certificate};

const models = join(__dirname, 'app/models');
const port = process.env.PORT || 3000;
const app = express();

const httpServer = http.createServer(app);
const httpsServer = https.createServer(options, app);

const multer  = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.ext);
    }
});

const upload = multer({
    fileFilter: function (req, file, cb) {
        switch (file.mimetype) {
            case 'text/html':
                file.ext = 'html';
                cb(null, true);
                break;
            case 'image/png':
                file.ext = 'png';
                cb(null, true);
                break;
            case 'image/jpeg':
                file.ext = 'jpeg';
                cb(null, true);
                break;
            case 'image/jpg':
                file.ext = 'jpg';
                cb(null, true);
                break;
            default:
                cb(null, false);
                break;
        }
    },
    storage: storage
});

/**
 * Expose
 */

module.exports = app;

// Bootstrap models
fs.readdirSync(models)
    .filter(file => ~file.search(/^[^.].*\.js$/))
    .forEach(file => require(join(models, file)));

const auto = require('./config/autocomplete');
auto.initialize();

// Bootstrap routes
require('./config/passport')(passport);
require('./config/express')(app, passport, upload);
require('./config/routes')(app, passport, auto, upload);

connect()
    .on('error', console.log)
    .on('disconnected', connect)
    .once('open', listen);

function listen () {
    if (app.get('env') === 'test') return;
    httpServer.listen(8080);
    httpsServer.listen(port);
    console.log('Express app started on port ' + port);
}

function connect () {
    const options = {
        keepAlive: 1,
        promiseLibrary: global.Promise
    };

    mongoose.connect(config.db, options);
    return mongoose.connection;
}
